﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DB1.ProvaTecnica.Domain.ValueObject;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DB1.ProvaTecnica.Domain.Tests.ValueObject
{
    [TestClass]
    public class SenhaScoreTests
    {
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SenhaScore_New_Erro_Null()
        {
            var senhaScore = new SenhaScore(null);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void SenhaScore_New_Erro_Branco()
        {
            var senhaScore = new SenhaScore("");
        }

        [TestMethod]
        public void SenhaScore_New_Valido_12345()
        {
            var senha = "12345";
            var scoreEsperado = 4;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(scoreEsperado, senhaScore.Total);
        }

        [TestMethod]
        public void SenhaScore_New_Valido_abcde()
        {
            var senha = "abcde";
            var scoreEsperado = 0;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(scoreEsperado, senhaScore.Total);
        }

        [TestMethod]
        public void SenhaScore_New_Valido_Medio()
        {
            var senha = "ALSK1029";
            var scoreEsperado = 50;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(scoreEsperado, senhaScore.Total);
        }

        [TestMethod]
        public void SenhaScore_New_Valido_Forte()
        {
            var senha = "xH~X].4d5/75-RP";
            var scoreEsperado = 100;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(scoreEsperado, senhaScore.Total);
        }

        [TestMethod]
        public void SenhaScore_Adicao_NumeroCaracteres()
        {
            var senha = "12345";
            var pontuacao = 20;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.NumeroCaracteres);
        }

        [TestMethod]
        public void SenhaScore_Adicao_LetrasMaiusculas()
        {
            var senha = "aBcDeFgH";
            var pontuacao = 8;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.LetrasMaiusculas);
        }

        [TestMethod]
        public void SenhaScore_Adicao_LetrasMinusculas()
        {
            var senha = "aBcDeFgH";
            var pontuacao = 8;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.LetrasMinusculas);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Numeros_SemOutroCaracter()
        {
            var senha = "12345";
            var pontuacao = 0;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.Numeros);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Numeros()
        {
            var senha = "12345.";
            var pontuacao = 20;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.Numeros);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Simbolos()
        {
            var senha = ".,/!?";
            var pontuacao = 30;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.Simbolos);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Numeros_Simbolos_Meio()
        {
            var senha = "a12!45z";
            var pontuacao = 10;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.NumerosSimbolosMeio);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Requisitos_Minimo_Atingido()
        {
            var senha = "Az1Az1Az1";
            var pontuacao = 8;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RequisitosMinimos);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Requisitos_Maximo_Atingido()
        {
            var senha = "Az1$Az1$";
            var pontuacao = 10;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RequisitosMinimos);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Requisitos_Nao_Atingidos_Tamanho()
        {
            var senha = "Az1$";
            var pontuacao = 0;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RequisitosMinimos);
        }

        [TestMethod]
        public void SenhaScore_Adicao_Requisitos_Nao_Atingidos_3de4()
        {
            var senha = "AzAAzAAzA";
            var pontuacao = 0;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RequisitosMinimos);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Somente_Letras()
        {
            var senha = "abcde";
            var pontuacao = -5;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.SomenteLetras);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Somente_Numeros()
        {
            var senha = "12345";
            var pontuacao = -5;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.SomenteNumeros);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Caracter_Repetido_aa()
        {
            var senha = "aa";
            var pontuacao = -4;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RepeteCaracteres);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Caracter_Repetido_aaa()
        {
            var senha = "aaa";
            var pontuacao = -14;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RepeteCaracteres);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Caracter_Repetido_aaaa()
        {
            var senha = "aaAAbbCCCdDDAA";
            var pontuacao = -29;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.RepeteCaracteres);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Letra_Maiuscula_Consecutiva()
        {
            var senha = "aaAAbbCCCdDDAA";
            var pontuacao = -12;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.LetraMaiusculaConsecutiva);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Letra_Minuscula_Consecutiva()
        {
            var senha = "abcDEF";
            var pontuacao = -4;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.LetraMinusculaConsecutiva);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Numeros_Consecutivos()
        {
            var senha = "112233";
            var pontuacao = -10;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.NumerosConsecutivos);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Letras_Sequenciais()
        {
            var senha = "abABCabcdABCDE";
            var pontuacao = -9;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.LetrasSequenciais);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Numeros_Sequenciais()
        {
            var senha = "0121232345";
            var pontuacao = -12;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.NumerosSequenciais);
        }

        [TestMethod]
        public void SenhaScore_Deducao_Simbolos_Sequenciais()
        {
            var senha = "!@#@#$%#$%¨&*(";
            var pontuacao = -12;
            var senhaScore = new SenhaScore(senha);
            Assert.AreEqual(pontuacao, senhaScore.SimbolosSequenciais);
        }
    }
}
