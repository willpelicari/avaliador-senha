﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DB1.ProvaTecnica.Helpers;

namespace DB1.ProvaTecnica.Domain.ValueObject
{
    public class SenhaScore
    {
        private int ComprimentoMinimoSenha = 8;
        public string Senha { get; private set; }

        public SenhaScore(string senha)
        {
            Validacao.ForNullOrEmpty(senha, "Senha");
            Senha = senha;

            //Adições
            SetNumeroCaracteres();
            SetLetrasMaiusculas();
            SetLetrasMinusculas();
            SetNumeros();
            SetSimbolos();
            SetNumerosSimbolosMeio();
            SetRequisitosMinimos();

            //Deduções
            SetSomenteLetras();
            SetSomenteNumeros();
            SetRepeteCaracteres();
            SetLetraMaiusculaConsecutiva();
            SetLetraMinusculaConsecutiva();
            SetNumerosConsecutivos();
            SetLetrasSequenciais();
            SetNumerosSequenciais();
            SetSimbolosSequenciais();

            //Total
            SetScoreTotal();
            SetComplexidade();
        }

        //Soma de todos os valores
        public int Total { get; private set; }

        //Complexidade Descrita
        public string Complexidade { get; set; }

        //Adições
        public int NumeroCaracteres { get; private set; }
        public int LetrasMaiusculas { get; private set; }
        public int LetrasMinusculas { get; private set; }
        public int Numeros { get; private set; }
        public int Simbolos { get; private set; }
        public int NumerosSimbolosMeio { get; private set; }
        public int RequisitosMinimos { get; private set; }

        //Deduções
        public int SomenteLetras { get; private set; }
        public int SomenteNumeros { get; private set; }
        public int RepeteCaracteres { get; private set; }
        public int LetraMaiusculaConsecutiva { get; private set; }
        public int LetraMinusculaConsecutiva { get; private set; }
        public int NumerosConsecutivos { get; private set; }
        public int LetrasSequenciais { get; private set; }
        public int NumerosSequenciais { get; private set; }
        public int SimbolosSequenciais { get; private set; }

        private void SetNumeroCaracteres()
        {
            NumeroCaracteres = Senha.Length * 4;
        }

        private void SetLetrasMaiusculas()
        {
            var quantidadeLetrasMaiusculas = Senha.Count(char.IsUpper);
            if (quantidadeLetrasMaiusculas > 0)
            {
                LetrasMaiusculas = (Senha.Length - quantidadeLetrasMaiusculas) * 2;
            }
            else
            {
                LetrasMaiusculas = 0;
            }
            
        }

        private void SetLetrasMinusculas()
        {
            var quantidadeLetrasMinusculas = Senha.Count(char.IsLower);
            if (quantidadeLetrasMinusculas > 0)
            {
                LetrasMinusculas = (Senha.Length - quantidadeLetrasMinusculas) * 2;
            }
            else
            {
                LetrasMinusculas = 0;
            }
            
        }

        private void SetNumeros()
        {
            var quantidadeNumeros = Senha.Count(char.IsNumber);

            if (quantidadeNumeros == Senha.Length)
            {
                Numeros = 0; //Não é dado pontuação para senhas só feitas de números
            }
            else
            {
                Numeros = quantidadeNumeros * 4;
            }
        }

        private void SetSimbolos()
        {
            var quantidadeSimbolos = Senha.Count(c => char.IsSymbol(c) || char.IsPunctuation(c));
            Simbolos = quantidadeSimbolos * 6;
        }

        private void SetNumerosSimbolosMeio()
        {
            if (Senha.Length <= 2)
            {
                NumerosSimbolosMeio = 0;
            }
            else
            {
                var quantidadeSimbolosMeio = Senha.Substring(1, Senha.Length - 2).Count( c => char.IsNumber(c) || char.IsSymbol(c) || char.IsPunctuation(c));
                NumerosSimbolosMeio = quantidadeSimbolosMeio * 2;
            }
                
        }

        private void SetRequisitosMinimos()
        {
            var requisitosAlcancados = 0;

            //Comprimento Mínimo Respeitado
            if (Senha.Length < ComprimentoMinimoSenha)
            {
                RequisitosMinimos = 0;
            }
            else
            {
                requisitosAlcancados++;

                //Contém Letra Maíscula
                if (Senha.Any(char.IsUpper))
                {
                    requisitosAlcancados++;
                }

                //Contém Letra Minúscula
                if (Senha.Any(char.IsLower))
                {
                    requisitosAlcancados++;
                }

                //Contém Letra
                if (Senha.Any(char.IsNumber))
                {
                    requisitosAlcancados++;
                }

                //Contém Símbolo
                if (Senha.Any(c => char.IsSymbol(c) || char.IsPunctuation(c)))
                {
                    requisitosAlcancados++;
                }

                if (requisitosAlcancados >= 4)
                {
                    RequisitosMinimos = requisitosAlcancados * 2;
                }
                else
                {
                    RequisitosMinimos = 0;
                }
            }
        }

        private void SetSomenteLetras()
        {
            if (Senha.Count(char.IsLetter) == Senha.Length)
            {
                SomenteLetras = -(Senha.Length);
            }
            else
            {
                SomenteLetras = 0;
            }
        }

        private void SetSomenteNumeros()
        {
            if (Senha.Count(char.IsNumber) == Senha.Length)
            {
                SomenteNumeros = -(Senha.Length);
            }
            else
            {
                SomenteNumeros = 0;
            }
        }

        private void SetRepeteCaracteres()
        {
            var deducaoIncremental = 0.00;
            var numeroCaracteresRepetidos = 0;
            var numeroCaracteresUnicos = 0; //Reduzido

            for (var i = 0; i < Senha.Length; i++)
            {
                var caracterExistente = false;
                for (var j = 0; j < Senha.Length; j++)
                {
                    if (Senha[i] != Senha[j] || i == j) continue;
                    caracterExistente = true;
                    deducaoIncremental += Math.Abs((double)Senha.Length / (j - i)); //Incrementa dedução a ser feita
                }

                if (caracterExistente)
                {
                    numeroCaracteresRepetidos++;
                    numeroCaracteresUnicos = Senha.Length - numeroCaracteresRepetidos;
                    deducaoIncremental = (numeroCaracteresUnicos > 0)
                        ? Math.Ceiling(deducaoIncremental / numeroCaracteresUnicos)
                        : Math.Ceiling(deducaoIncremental);
                }
            }

            RepeteCaracteres = -(Convert.ToInt32(deducaoIncremental));
        }

        private void SetLetraMaiusculaConsecutiva()
        {
            int indexTemporaria = -1;
            int contadorConsecutivo = 0;

            for (var i = 0; i < Senha.Length; i++)
            {
                if (char.IsUpper(Senha[i]))
                {
                    if (indexTemporaria >= 0 && indexTemporaria + 1 == i)
                    {
                        contadorConsecutivo++;
                    }
                    indexTemporaria = i;
                }
            }

            LetraMaiusculaConsecutiva = -(contadorConsecutivo * 2);
        }

        private void SetLetraMinusculaConsecutiva()
        {
            int indexTemporaria = -1;
            int contadorConsecutivo = 0;

            for (var i = 0; i < Senha.Length; i++)
            {
                if (char.IsLower(Senha[i]))
                {
                    if (indexTemporaria >= 0 && indexTemporaria + 1 == i)
                    {
                        contadorConsecutivo++;
                    }
                    indexTemporaria = i;
                }
            }

            LetraMinusculaConsecutiva = -(contadorConsecutivo * 2);
        }

        private void SetNumerosConsecutivos()
        {
            int indexTemporaria = -1;
            int contadorConsecutivo = 0;

            for (var i = 0; i < Senha.Length; i++)
            {
                if (char.IsNumber(Senha[i]))
                {
                    if (indexTemporaria >= 0 && indexTemporaria + 1 == i)
                    {
                        contadorConsecutivo++;
                    }
                    indexTemporaria = i;
                }
            }

            NumerosConsecutivos = -(contadorConsecutivo * 2);
        }

        private void SetLetrasSequenciais()
        {
            if (Senha.Length < 3)
            {
                LetrasSequenciais = 0;
            }
            else
            {
                var alfabeto = "abcdefghijklmnopqrstuvwxyz";
                var contadorSequencia = 0;
                for (var i = 0; i < 23; i++)
                {
                    var ordemCrescente = alfabeto.Substring(i, 3);
                    var ordemInversa = String.Concat(ordemCrescente.Reverse());
                    if (Senha.ToLower().IndexOf(ordemCrescente) != -1 || Senha.ToLower().IndexOf(ordemInversa) != -1)
                    {
                        contadorSequencia++;
                    }
                }

                LetrasSequenciais = -(contadorSequencia * 3);
            }
        }

        private void SetNumerosSequenciais()
        {
            if (Senha.Length < 3)
            {
                NumerosSequenciais = 0;
            }
            else
            {
                var sequenciaNumerica = "01234567890";
                var contadorSequencia = 0;
                for (var i = 0; i < 8; i++)
                {
                    var ordemCrescente = sequenciaNumerica.Substring(i, 3);
                    var ordemInversa = String.Concat(ordemCrescente.Reverse());
                    if (Senha.ToLower().IndexOf(ordemCrescente) != -1 || Senha.ToLower().IndexOf(ordemInversa) != -1)
                    {
                        contadorSequencia++;
                    }
                }

                NumerosSequenciais = -(contadorSequencia * 3);
            }
        }

        private void SetSimbolosSequenciais()
        {
            if (Senha.Length < 3)
            {
                SimbolosSequenciais = 0;
            }
            else
            {
                var sequenciaSimbolos = ")!@#$%^&*()";
                var contadorSequencia = 0;
                for (var i = 0; i < 8; i++)
                {
                    var ordemCrescente = sequenciaSimbolos.Substring(i, 3);
                    var ordemInversa = String.Concat(ordemCrescente.Reverse());
                    if (Senha.ToLower().IndexOf(ordemCrescente) != -1 || Senha.ToLower().IndexOf(ordemInversa) != -1)
                    {
                        contadorSequencia++;
                    }
                }

                SimbolosSequenciais = -(contadorSequencia * 3);
            }
        }

        private void SetScoreTotal()
        {
            var total = NumeroCaracteres +
                LetrasMaiusculas +
                LetrasMinusculas +
                Numeros +
                Simbolos +
                NumerosSimbolosMeio +
                RequisitosMinimos +
                SomenteLetras +
                SomenteNumeros +
                RepeteCaracteres +
                LetraMaiusculaConsecutiva +
                LetraMinusculaConsecutiva +
                NumerosConsecutivos +
                LetrasSequenciais +
                NumerosSequenciais +
                SimbolosSequenciais;

            if (total < 0)
            {
                Total = 0;
            }
            else if (total > 100)
            {
                Total = 100;
            }
            else
            {
                Total = total;
            }
        }

        private void SetComplexidade()
        {
            if (Total >= 0 && Total < 20)
            {
                Complexidade = "Muito Fraca";
            }
            else if (Total >= 20 && Total < 40)
            {
                Complexidade = "Fraca";
            }
            else if (Total >= 40 && Total < 60)
            {
                Complexidade = "Boa";
            }
            else if (Total >= 60 && Total < 80)
            {
                Complexidade = "Forte";
            }
            else if (Total >= 80 && Total <= 100)
            {
                Complexidade = "Muito Forte";
            }
        }
    }
}
