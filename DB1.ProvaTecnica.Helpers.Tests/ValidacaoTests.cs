﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DB1.ProvaTecnica.Helpers.Tests
{
    [TestClass]
    public class ValidacaoTests
    {
        private string ValorTeste { get; set; }
        private int MinLength { get; set; }
        private int MaxLength { get; set; }

        public ValidacaoTests()
        {
            ValorTeste = "12345";
            MinLength = 0;
            MaxLength = 30;
        }

        [TestMethod]
        public void Validacao_ForNullOrEmpty_Valido()
        {
            Validacao.ForNullOrEmpty(ValorTeste, "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Validacao_ForNullOrEmpty_Erro_Branco()
        {
            Validacao.ForNullOrEmpty("", "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Validacao_ForNullOrEmpty_Erro_Nulo()
        {
            Validacao.ForNullOrEmpty(null, "");
        }

        [TestMethod]
        public void Validacao_ForStringLength_Valido()
        {
            Validacao.ForStringLength(ValorTeste, MinLength, MaxLength, "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Validacao_ForStringLength_Erro_MenorValorMinimo()
        {
            MinLength = 6;
            Validacao.ForStringLength(ValorTeste, MinLength, MaxLength, "");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Validacao_ForStringLength_Erro_MaiorValorMaximo()
        {
            MaxLength = 4;
            Validacao.ForStringLength(ValorTeste, MinLength, MaxLength, "");
        }
    }
}
