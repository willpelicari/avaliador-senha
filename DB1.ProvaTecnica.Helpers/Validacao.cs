﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB1.ProvaTecnica.Helpers
{
    public class Validacao
    {
        public static void ForNullOrEmpty(string valor, string propName)
        {
            if (String.IsNullOrEmpty(valor))
            {
                throw new Exception($"{propName} não pode ser vazio ou nulo.");
            }
        }

        public static void ForStringLength(string valor, int minLength, int maxLength, string propName)
        {
            if (valor.Length < minLength || valor.Length > maxLength)
            {
                throw new Exception($"{propName} deve ter de {minLength} à {maxLength} caracteres.");
            }
        }
    }
}
