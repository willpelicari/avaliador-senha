﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DB1.ProvaTecnica.Domain.ValueObject;
using DB1.ProvaTecnica.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace DB1.ProvaTecnica.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Senha")]
    public class SenhaController : Controller
    {
        // POST: api/Senha/Avaliar
        [HttpPost]
        [Route("Avaliar")]
        public IActionResult Post([FromBody]SenhaScoreRequestDTO solicitacao)
        {
            if (solicitacao == null)
            {
                return BadRequest("Não foi possível ler objeto JSON de entrada.");
            }

            if (string.IsNullOrEmpty(solicitacao.Senha))
            {
                return BadRequest("'Senha' é um campo obrigatório.");
            }

            var senha = new SenhaScore(solicitacao.Senha);
            var senhaSimples = new SenhaScoreSimplesResponseDTO(senha.Total, senha.Complexidade);
            return Ok(senhaSimples);
        }

        // POST: api/Senha/AvaliarDetalhado
        [HttpPost]
        [Route("AvaliarDetalhado")]
        public IActionResult PostDetalhado([FromBody]SenhaScoreRequestDTO solicitacao)
        {
            if (solicitacao == null)
            {
                return BadRequest("Não foi possível ler objeto JSON de entrada.");
            }

            if (string.IsNullOrEmpty(solicitacao.Senha))
            {
                return BadRequest("'Senha' é um campo obrigatório.");
            }

            var senha = new SenhaScore(solicitacao.Senha);
            var senhaDetalhado = new SenhaScoreDetalhadoResponseDTO(senha);
            return Ok(senhaDetalhado);
        }
    }
}
