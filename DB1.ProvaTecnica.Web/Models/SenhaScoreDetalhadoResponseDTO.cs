﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DB1.ProvaTecnica.Domain.ValueObject;

namespace DB1.ProvaTecnica.Web.Models
{
    public class SenhaScoreDetalhadoResponseDTO
    {
        public string Total { get; set; }
        public string Complexidade { get; set; }
        //Adições
        public int NumeroCaracteres { get; private set; }
        public int LetrasMaiusculas { get; private set; }
        public int LetrasMinusculas { get; private set; }
        public int Numeros { get; private set; }
        public int Simbolos { get; private set; }
        public int NumerosSimbolosMeio { get; private set; }
        public int RequisitosMinimos { get; private set; }

        //Deduções
        public int SomenteLetras { get; private set; }
        public int SomenteNumeros { get; private set; }
        public int RepeteCaracteres { get; private set; }
        public int LetraMaiusculaConsecutiva { get; private set; }
        public int LetraMinusculaConsecutiva { get; private set; }
        public int NumerosConsecutivos { get; private set; }
        public int LetrasSequenciais { get; private set; }
        public int NumerosSequenciais { get; private set; }
        public int SimbolosSequenciais { get; private set; }

        public SenhaScoreDetalhadoResponseDTO(SenhaScore senha)
        {
            NumeroCaracteres = senha.NumeroCaracteres;
            LetrasMaiusculas = senha.LetrasMaiusculas;
            LetrasMinusculas = senha.LetrasMinusculas;
            Numeros = senha.Numeros;
            Simbolos = senha.Simbolos;
            NumerosSimbolosMeio = senha.NumerosSimbolosMeio;
            RequisitosMinimos = senha.RequisitosMinimos;
            SomenteLetras = senha.SomenteLetras;
            SomenteNumeros = senha.SomenteNumeros;
            RepeteCaracteres = senha.RepeteCaracteres;
            LetraMaiusculaConsecutiva = senha.LetraMaiusculaConsecutiva;
            LetraMinusculaConsecutiva = senha.LetraMinusculaConsecutiva;
            NumerosConsecutivos = senha.NumerosConsecutivos;
            LetrasSequenciais = senha.LetrasSequenciais;
            NumerosSequenciais = senha.NumerosSequenciais;
            SimbolosSequenciais = senha.SimbolosSequenciais;
            Total = $"{senha.Total}%";
            Complexidade = senha.Complexidade;
        }
    }
}
