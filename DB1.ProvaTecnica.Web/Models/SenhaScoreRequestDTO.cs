﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DB1.ProvaTecnica.Web.Models
{
    public class SenhaScoreRequestDTO
    {
        public string Senha { get; set; }
    }
}
