﻿namespace DB1.ProvaTecnica.Web.Models
{
    public class SenhaScoreSimplesResponseDTO
    {
        public string Score { get; private set; }
        public string Complexidade { get; private set; }

        public SenhaScoreSimplesResponseDTO(int valorTotal, string complexidade)
        {
            Score = $"{valorTotal}%";
            Complexidade = complexidade;
        }
    }
}
