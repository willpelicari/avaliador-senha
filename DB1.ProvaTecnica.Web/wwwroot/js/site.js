﻿// Write your JavaScript code.
$(document).ready(function () {
    $('#senha').keyup(function () {

        var senha = {
            Senha: $('#senha').val()
        };

        $.ajax({
            type: "POST",
            url: "api/Senha/Avaliar",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(senha),
            success: function (data) {
                console.log(data);
                //score
                $("#porcentagem").text(data.score);
                //complexidade
                $("#complexidade").text(data.complexidade);
                //cores
                switch (data.complexidade) {
                case "Muito Fraca":
                        $("#complexidade").css("background-color", "#b94a48");
                    break;
                case "Fraca":
                    $("#complexidade").css("background-color", "#f00");
                    break;
                case "Boa":
                        $("#complexidade").css("background-color", "#f89406");
                        break;
                case "Forte":
                        $("#complexidade").css("background-color", "#468847");
                    break;
                case "Muito Forte":
                        $("#complexidade").css("background-color", "#3a87ad");
                    break;
                default:
                    $("#complexidade").css("background-color", "#888");
                    break;
                }
            },
            error: function (error) {

            }
        });
    });
});